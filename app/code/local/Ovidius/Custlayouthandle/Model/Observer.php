<?php
class Ovidius_Custlayouthandle_Model_Observer {
    public function controllerActionLayoutLoadBefore($observer)
    {
        $category = Mage::registry('current_category');
        if ($category instanceof Mage_Catalog_Model_Category) {
            $update = Mage::getSingleton('core/layout')->getUpdate();
            $children = ($category->getChildrenCategories()->getData()) ? 'has_children' : 'no_children';
            $update->addHandle('catalog_category_' . $children);
        }
        return $this;
    }
}
