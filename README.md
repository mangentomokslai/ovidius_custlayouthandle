# README

This module demostrates how to add custom layout handle. 

catalog_category_has_children and catalog_category_no_children are added for category pages with and without children categories respectively.

Blocks with text "This is category without children" and "This is category with children" are added for each pages in layout xml.